^keys.+$|keys/%s
^inkscape-man.+$|man/%s
^inkview-man.+$|man/%s
^(.+/tutorial-[a-z]+)\.html$|tutorials/%s.en.html
^.+/tutorial.+$|tutorials/%s
^keys/keys(\.[A-Za-z_]+)?.html$|keys/keys100%s.html
^man/inkscape-man(\.[A-Za-z_]+)?.html$|man/inkscape-man100%s.html
^man/inkview-man(\.[A-Za-z_]+)?.html$|man/inkview-man100%s.html
